﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BotoesScript : MonoBehaviour
{

	private IEnumerator Agurdar (int segundos)
	{
		yield return new WaitForSeconds (segundos);
		GameObject.FindGameObjectWithTag ("TextoMarcadorSelecao").GetComponent<Text> ().text = string.Empty;
	}

	public void VoltarTelaInicial ()
	{
		Application.LoadLevel ("TelaInicial");
	}

	public void Sobre ()
	{
		Application.OpenURL ("http://www.inf.furb.br/gcg/visedu/");	
	}

	public void EducacaoNoTransito ()
	{
		Application.OpenURL ("http://www.inf.furb.br/gcg/tecedu/transito/educa.pdf");
	}

	public void Imprimir ()
	{
		Application.OpenURL ("http://www.inf.furb.br/gcg/tecedu/transito/mapa.pdf");
	}

	public void Jogar ()
	{
		Application.LoadLevel ("Cena_01");
	}
}
