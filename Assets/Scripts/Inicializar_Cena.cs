﻿using UnityEngine;
using System.Collections;

public class Inicializar_Cena : MonoBehaviour {


	// Use this for initialization
	void Start () {

		GameObject casa_01;
		GameObject casa_02;
		GameObject oficina;
		GameObject postoCombustivel;

		casa_01 = GameObject.Find ("Casa_01");
		casa_02 = GameObject.Find ("Casa_02");
		oficina = GameObject.Find ("Oficina");
		postoCombustivel = GameObject.Find ("Posto_Combustivel");

		casa_01.transform.localScale = new Vector3 (0,0,0);
		casa_02.transform.localScale = new Vector3 (0,0,0);
		oficina.transform.localScale = new Vector3 (0,0,0);
		postoCombustivel.transform.localScale = new Vector3 (0,0,0);

		exibirObjetos (casa_02);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void exibirObjetos(GameObject casa_02){

		float x = 0;

		while (casa_02.transform.localScale.x < 1.164966f) {
			x += 000010f;
			casa_02.transform.position = new Vector3 (x, x, x);
		}
	}
}
