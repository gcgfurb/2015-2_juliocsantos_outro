﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
using Vuforia;
using UnityEngine.UI;

public class CarroComJoystickScript : MonoBehaviour
{
	private ControleGUIs Barras;
	private bool aguardarArrumarCarro = false;
	private bool aguardarEncherTanque = false;
	private const float speed = 90;
	private const float rotationSpeed = 50;

	// Use this for initialization
	void Start ()
	{

		this.Barras = FindObjectOfType<ControleGUIs> ();
	}

	// Update is called once per frame
	void Update ()
	{
		//GameObject.FindGameObjectWithTag ("FPS").GetComponent<Text> ().text = ((int)(1.0f / Time.smoothDeltaTime)).ToString ();

		float movimentoHorizontal = CrossPlatformInputManager.GetAxis ("Horizontal");
		float movimentoVertical = CrossPlatformInputManager.GetAxis ("Vertical");
		// Rotaçao
		if (movimentoHorizontal != 0 && (movimentoHorizontal > 0.30f || movimentoHorizontal < -0.30f)) {
			float rotation = movimentoHorizontal * rotationSpeed;
			rotation *= Time.deltaTime;

			if (movimentoHorizontal < 0 && movimentoVertical < 0 ||
			    movimentoHorizontal > 0 && movimentoVertical < 0) {
				transform.Rotate (0, rotation * -1, 0);
			} else {
				transform.Rotate (0, rotation, 0);
			}
		}
		//Translaçao
		if (movimentoVertical != 0) {
			float translation = movimentoVertical * speed;
			translation *= Time.deltaTime;
			transform.Translate (0, 0, translation);
		}
		//Mantem a posiçao y fixa devido a rotaçao
		if (movimentoHorizontal != 0 || movimentoVertical != 0) {
			transform.localPosition = new Vector3 (transform.localPosition.x, 0.0087f, transform.localPosition.z);
			Barras.DiminuiGasolinaAtual (0.15f);
		}

		if (Barras.gasolinaAtual <= 0) {
			Destroy (gameObject);
		}
	}

	// Permite colidir com o Objeto
	void OnCollisionEnter (Collision ObjetoColidido)
	{
		if (ObjetoColidido.gameObject.tag == "predio") {
			this.Barras.DiminuiMecanicaAtual (3);
		}
		if (ObjetoColidido.gameObject.tag == "arvore") {
			this.Barras.DiminuiMecanicaAtual (1);
		}
		if (Barras.mecanicaAtual <= 0) {
			Destroy (gameObject);
		}
	}

	// Intercesao nao deixa colidir fisica
	void OnTriggerEnter (Collider ObjetoColidido)
	{
		if (ObjetoColidido.gameObject.tag == "Buraco") {
			this.Barras.DiminuiMecanicaAtual (2);
		}
		if (Barras.mecanicaAtual <= 0) {
			Destroy (gameObject);
		}
	}
	void OnTriggerStay (Collider ObjetoColidido)
	{
		switch (ObjetoColidido.gameObject.tag) {
		case "arrumar":
			if (!this.aguardarArrumarCarro) {
				StartCoroutine (ArrumarCarro ());
			}
			break;
		case "abastecer":
			if (!this.aguardarEncherTanque) {
				StartCoroutine (EncherTanqueGasolina ());
			}
			break;
		}
			
		if (Barras.mecanicaAtual <= 0) {
			Destroy (gameObject);
		}	    
	}

	private IEnumerator EncherTanqueGasolina ()
	{
		this.Barras.IncrementaGasolinaAtual (20);
		this.aguardarEncherTanque = true;
		yield return new WaitForSeconds (2);
		this.aguardarEncherTanque = false;
	}
	
	private IEnumerator ArrumarCarro ()
	{
		this.Barras.IncrementaMecanicaAtual (4);
		this.aguardarArrumarCarro = true;
		yield return new WaitForSeconds (2);
		this.aguardarArrumarCarro = false;
	}
}
